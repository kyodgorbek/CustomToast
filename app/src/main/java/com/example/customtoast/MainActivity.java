package com.example.customtoast;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    /*
    * 1. To create an XML layout for our custom Toast
    *
    * 2. To Inflate  the layout using LayoutInflater
    *
    * 3. To Create a Toast and assign it the layout of Custom Toast
    * */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Button Click Handler
    public void showCustomToast(View v) {

        LayoutInflater inflater = getLayoutInflater();
        View appearance
        inflater.inflate(R.layout.custom layout, (ViewGroup) findViewById(R.layout.root layout));
        Toast toast = new Toast(MainActivity.this);

        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);

        toast.setView(appearance);
        toast.show();
    }

}